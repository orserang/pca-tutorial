from pca import *

# Food quantities consumed by a handful of European countries
europeanFoodConsumption = Dataset('EuropeanFoodData.tsv')
europeanFoodConsumption.drawCovarianceMatrix()
plt.show()

