import networkx as nx
import numpy as np
import math

# Note: doesn't skip tabs within quotes
class TSV:
    @classmethod
    def removeQuotationMarks(cls, string):
        return string.replace('"', '')

    def __init__(self, fname):
        self.loadFromFile(fname)

    def loadFromFile(self, fname):
        f = open(fname)
        rows = [ line.replace('\n','').split('\t') for line in f.readlines() ]

        self.headerRow = rows[0]
        self.labelToIndex = dict( [ (item, ind) for ind, item in enumerate( self.headerRow ) ] )
        nonHeaderRows = rows[1:]

        self.nonHeaderRows = [ [ self.removeQuotationMarks(val) for val in row ] for row in nonHeaderRows ]

    def multiColumnsByLabel(self, labelList):
        return zip( *[self.columnByLabel(label) for label in labelList] )

    def columnByLabel(self, label):
        index = self.labelToIndex[label]
        return [ row[index] for row in self.nonHeaderRows ]
