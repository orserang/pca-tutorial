from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import proj3d
import pylab
import numpy as np
import math
import itertools

from TSV import *

def flatten(listOfLists):
    "Flatten one level of nesting"
    return itertools.chain.from_iterable(listOfLists)

def toNumericVector(vec):
    return np.array([ float(val) for val in vec ])

def updatePosition(ax, fig, label, point, e):
    x2, y2, _ = proj3d.proj_transform( *point, M=ax.get_proj() )
    label.xy = x2,y2
    label.update_positions(fig.canvas.renderer)

def updateAllPositions(ax, fig, allLabels, allPoints, e):
    for label, point in zip(allLabels, allPoints):
        updatePosition(ax, fig, label, point, e)
    fig.canvas.draw()

def norm(vector):
    return math.sqrt(sum(vector*vector))

def mean(vector):
    return sum(vector) / float(len(vector))

def stdDev(vector):
    mu = mean(vector)
    return math.sqrt(sum( (vector - mu)*(vector - mu) ) / (len(vector)-1))

def drawVector(ax, mean, vector, color):
    xStartEnd = ( mean[0], vector[0] )
    yStartEnd = ( mean[1], vector[1] )
    zStartEnd = ( mean[2], vector[2] )
    ax.plot(xStartEnd, yStartEnd, zStartEnd, color=color)

def stretchingBoundary(minimumVector, maximumVector, meanVector, directionVector):
    allScales = []
    for i in range(len(minimumVector)):
        normConstantForAxis = lambda meanVal, val, maxVal : (maxVal - meanVal) /  val
        # one of these must be negative and one must be positive (or both could be infinite)
        forward = normConstantForAxis(meanVector[i], directionVector[i], maximumVector[i])
        backward = normConstantForAxis(meanVector[i], directionVector[i], minimumVector[i])
        scaleForAxis = max( [ forward, backward ] )

        allScales.append(scaleForAxis)

    return min(allScales)

class Dataset(TSV):
    def __init__(self, fname):
        TSV.__init__(self, fname)
        tsv = TSV(fname)
        self.initPrincipalComponentsAndMeans()

    def covarianceMatrix(self):
        matrix = []
        for rowName in self.headerRow[1:]:
            rowVector = np.array(toNumericVector(self.columnByLabel(rowName)))
            row = []
            for colName in self.headerRow[1:]:
                colVector = np.array(toNumericVector(self.columnByLabel(colName)))
                cov = lambda A, B : sum( (A - mean(A)) * (B - mean(B)) ) / (len(A)-1)
                covariance = cov(rowVector, colVector)
                correlation = covariance / (stdDev(rowVector) * stdDev(colVector))

#                row.append(covariance)
                row.append(correlation)

            matrix.append(row)

        return matrix

    def initPrincipalComponentsAndMeans(self):
        matrix = self.covarianceMatrix()
        eigenvalues, eigenvectors = np.linalg.eigh(matrix)
        eigenvectors = np.transpose(eigenvectors)
        self.orderedEigenvalues = []
        self.orderedEigenvectors = []
        for eVal, eVec in sorted(zip(eigenvalues, eigenvectors))[::-1]:
            self.orderedEigenvalues.append(eVal)
            self.orderedEigenvectors.append(eVec)

        means = []
        for rowName in self.headerRow[1:]:
            rowVector = np.array(toNumericVector(self.columnByLabel(rowName)))
            means.append( mean(rowVector) )

        self.means = means

    def drawCovarianceMatrix(self):
        matrix = self.covarianceMatrix()

        plt.matshow(matrix, interpolation='none')
        plt.yticks(range(len(matrix)), self.headerRow[1:])
        xLocs, xLabels = plt.xticks(range(len(matrix)), self.headerRow[1:])
        plt.setp(xLabels, rotation=90)
        plt.colorbar()

    def draw3d(self, xName, yName, zName, shouldDrawPCs = False, shouldDrawLabels = False):
        fig = plt.figure(1)
        ax = fig.add_subplot(111, projection='3d')

        X = toNumericVector(self.columnByLabel(xName))
        Y = toNumericVector(self.columnByLabel(yName))
        Z = toNumericVector(self.columnByLabel(zName))

        ax.set_xlabel(xName)
        ax.set_ylabel(yName)
        ax.set_zlabel(zName)

        ax.scatter(X,Y,Z, color='purple', marker='o', s=200)

        if shouldDrawPCs:
            ##### draw a line for each PC
            # subtract 1 because column has point names
            meansForVariablesDrawn = np.array([ self.means[ self.labelToIndex[ variableString ] -1 ] for variableString in (xName, yName, zName) ])

            pcVectorsToDraw = []
            # get the top 3 PCs:
            for i in range(3):
                pcVectorsToDraw.append(self.orderedEigenvalues[i] * np.array( [ self.orderedEigenvectors[i][ self.labelToIndex[ variableString ] -1 ] for variableString in (xName, yName, zName) ] ))

            # find the maximum scaling factor to draw all PCs with same relative length
            axisMinima = np.array([ ax.get_xlim()[0], ax.get_ylim()[0], ax.get_zlim()[0] ])
            axisMaxima = np.array([ ax.get_xlim()[1], ax.get_ylim()[1], ax.get_zlim()[1] ])
            scalingFactor = min( [ stretchingBoundary(axisMinima, axisMaxima, meansForVariablesDrawn, pcVectorsToDraw[i]) for i in range(3) ] )

            for pcVec, color in zip(pcVectorsToDraw, ('red', 'green', 'blue')):
                drawVector(ax, meansForVariablesDrawn, meansForVariablesDrawn + scalingFactor * pcVec, color=color)

        if shouldDrawLabels:
            allLabels, allPoints = self.drawLabels3d(ax, fig, X, Y, Z)
            updateForMyAxes = lambda e : updateAllPositions(ax, fig, allLabels, allPoints, e)
            fig.canvas.mpl_connect('motion_notify_event', updateForMyAxes)

    def drawLabels3d(self, ax, fig, X, Y, Z):
        allLabels = []
        allPoints = list(zip(X,Y,Z))
        for name, point in zip( self.pointNames(), allPoints ):
            xProj, yProj, zProj = proj3d.proj_transform( *point, M=ax.get_proj() )
            label = pylab.annotate(
                name, 
                xy = (xProj, yProj),
                xytext = (-20, 20),
                textcoords = 'offset points',
                ha = 'right',
                va = 'bottom',
                bbox = dict(boxstyle = 'round,pad=0.5',
                            fc = 'yellow',
                            alpha = 0.2),
                arrowprops = dict(arrowstyle = '->',
                                  connectionstyle = 'arc3,rad=0'))

            allLabels.append(label)
        return allLabels, allPoints

    @classmethod
    def cosineSimilarity(cls, vec, axis):
        return sum(vec * axis) / (norm(axis))
    # use first two PCs
    def draw2dScores(self, pcAIndex, pcBIndex, shouldDrawLabels = False):
        fig = plt.figure(2)
        ax = fig.add_subplot(111)
        
        pc0Proj = []
        pc1Proj = []
        for row in self.nonHeaderRows:
            vec = toNumericVector(row[1:]) - self.means
            # since PCs are ortho, don't bother projecting out of pcA before pcB
            x = self.cosineSimilarity(vec, self.orderedEigenvectors[pcAIndex])
            y = self.cosineSimilarity(vec, self.orderedEigenvectors[pcBIndex])
            pc0Proj.append(x)
            pc1Proj.append(y)

        ax.scatter(pc0Proj, pc1Proj, color='purple', marker='o', s=200)
        ax.set_xlabel('Principal component ' + str(pcAIndex))
        ax.set_ylabel('Principal component ' + str(pcBIndex))

        if shouldDrawLabels:
            allPoints = list(zip(pc0Proj, pc1Proj))
            for name, point in zip( self.pointNames(), allPoints ):
                pylab.annotate(name, 
                    xy = point,
                    xytext = (-20, 20),
                    textcoords = 'offset points',
                    ha = 'right',
                    va = 'bottom',
                    bbox = dict(boxstyle = 'round,pad=0.5',
                                fc = 'yellow',
                                alpha = 0.2),
                    arrowprops = dict(arrowstyle = '->',
                                      connectionstyle = 'arc3,rad=0'))

    # use first two PCs
    def draw2dLoadings(self, pcAIndex, pcBIndex, shouldDrawLabels = False):
        fig = plt.figure(3)
        ax = fig.add_subplot(111)
        
        pc0Series = []
        pc1Series = []

        # TODO: ?
        pc0 = self.orderedEigenvalues[pcAIndex]*self.orderedEigenvectors[pcAIndex]
        pc1 = self.orderedEigenvalues[pcBIndex]*self.orderedEigenvectors[pcBIndex]

        for variable in self.headerRow[1:]:
            index = self.labelToIndex[variable]-1
            pc0Series.append(pc0[index] / norm(pc0))
            pc1Series.append(pc1[index] / norm(pc1))

        ax.scatter(pc0Series, pc1Series, color='purple', marker='o', s=200)
        ax.set_xlabel('Principal component ' + str(pcAIndex))
        ax.set_ylabel('Principal component ' + str(pcBIndex))

        if shouldDrawLabels:
            allPoints = list(zip(pc0Series, pc1Series))
            for name, point in zip( self.headerRow[1:], allPoints ):
                pylab.annotate(name, 
                    xy = point,
                    xytext = (-20, 20),
                    textcoords = 'offset points',
                    ha = 'right',
                    va = 'bottom',
                    bbox = dict(boxstyle = 'round,pad=0.5',
                                fc = 'yellow',
                                alpha = 0.2),
                    arrowprops = dict(arrowstyle = '->',
                                      connectionstyle = 'arc3,rad=0'))

    def pointNames(self):
        return [ row[0] for row in self.nonHeaderRows ]

# def update_position(e):
#     x2, y2, _ = proj3d.proj_transform(1,1,1, ax.get_proj())
#     label.xy = x2,y2
#     label.update_positions(fig.canvas.renderer)
#     fig.canvas.draw()

#fig.canvas.mpl_connect('button_release_event', update_position)
# fig.canvas.mpl_connect('motion_notify_event', update_position)
# pylab.show()
