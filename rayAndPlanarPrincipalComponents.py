from pca import *

# With principal components
narrowRay = Dataset('narrowRay.tsv')
narrowRay.draw3d('X','Y','Z',True)
plt.show()

planar = Dataset('planarGalaxy.tsv')
planar.draw3d('X','Y','Z',True)
plt.show()
