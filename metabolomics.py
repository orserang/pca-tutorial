from pca import *

metabolomics = Dataset('metabolomics.tsv')
metabolomics.draw2dScores(0,1,True)
metabolomics.draw2dLoadings(0,1,True)
plt.show()

metabolomics.draw2dScores(0,2,True)
metabolomics.draw2dLoadings(0,2,True)
plt.show()

metabolomics.draw3d('m/z=678.1','m/z=624.6','m/z=141.1',True,True)
plt.show()
