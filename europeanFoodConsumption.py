from pca import *

# Food quantities consumed by a handful of European countries
europeanFoodConsumption = Dataset('EuropeanFoodData.tsv')
europeanFoodConsumption.draw3d('Coffee','Tea','Butter',False,False)
plt.show()

europeanFoodConsumption.draw2dScores(0,1,True)
europeanFoodConsumption.draw2dLoadings(0,1,True)
plt.show()

europeanFoodConsumption.draw2dScores(0,2,True)
europeanFoodConsumption.draw2dLoadings(0,2,True)
plt.show()

europeanFoodConsumption.draw3d('Garlic','FrozenFish','Tea',True,True)
plt.show()
