import numpy
from mpl_toolkits.mplot3d import proj3d
import matplotlib.pyplot as plt
import pylab  

def update_position(e):
    print "From update position"
    #Transform co-ordinates to get new 2D projection
    tX, tY, _ = proj3d.proj_transform(dataX, dataY, dataZ, ax.get_proj())
    for i in range(len(dataX)):
        label = labels[i]
        label.xy = tX[i],tY[i]
        label.update_positions(fig.canvas.renderer)
    fig.canvas.draw()
    return

#Input 3D Data
data = numpy.array([[3,6,2],[4,6,2],[2,9,2],[3,6,10],[6,1,5]])

#Separate into X, Y, Z for greater clarity
dataX = data[:,0]
dataY = data[:,1]
dataZ = data[:,2]

plt.close()
fig = plt.figure()
ax = fig.gca(projection='3d')

#3D scatter plot
ax.scatter(dataX, dataY, dataZ, marker = 'o', c='b')

#Transform co-ordinates to get initial 2D projection
tX, tY, _ = proj3d.proj_transform(dataX, dataY, dataZ, ax.get_proj())

#Array of labels
labels = []

#Loop through data points to initially annotate scatter plot
#and populate labels array
for i in range(len(dataX)):
    text='['+str(int(dataX[i]))+','+str(int(dataY[i]))+','+str(int(dataZ[i]))+']'
    label = ax.annotate(text,
            xycoords='data',
            xy = (tX[i], tY[i]), xytext = (-20, 20),
            textcoords = 'offset points', ha = 'right', va = 'top', fontsize=6,
            bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.5),
            arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))
    labels.append(label)
#Positions are updated when mouse button is released after rotation.
fig.canvas.mpl_connect('button_release_event', update_position)
fig.show()
